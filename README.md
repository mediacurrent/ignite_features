# Mediacurrent Ignite Install Profile #

The Mediacurrent Ignite install profile adds out-of-the-box editorial, administrative and media enhancements to the typical Drupal 8 installation.
This project adds additional content features to be used in conjunction with the
base installation profile.

### Optional features
* [ignite_blocks](https://bitbucket.org/mediacurrent/ignite_features/src/4.x/modules/ignite_blocks/) - Provides common block types.
* [ignite_content](https://bitbucket.org/mediacurrent/ignite_features/src/4.x/modules/ignite_content/) - Provides common content types.
* [ignite_paragraphs](https://bitbucket.org/mediacurrent/ignite_features/src/4.x/modules/ignite_paragraphs/) - Provides common paragraphs
* ignite_search - Enables Search API search index and page
